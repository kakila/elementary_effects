# Global sensitvity via elementary effects

GNU Octave implementation of elementary effect for variable screening using 
global sensitivity based on derivatives.


## Installation

The code was developed using GNU Octave 6.2.0, you can get the latest stable
from [the download page](https://www.gnu.org/software/octave/download.html)

Once you have octave running, from its prompt execute

```
pkg install https://gitlab.com/kakila/elementary_effects/-/archive/master/elementary_effects-master.tar.gz
```

to install the latest (development) version of this package.
Once installed load the package

```
pkg load elemeff
```

and test it by runing one of the demos, e.g.

```
demo elementary_effects
```


You can aslo generate reports using the function `publish`, e.g.

```
publish elemeff_script_compare_sampling.m
```

which is also hosted [here]()


