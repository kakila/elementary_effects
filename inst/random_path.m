## Copyright (C) 2021 JuanPi Carbajal
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @defun {@var{P} =} random_path(@var{ndim})
## @defunx {@var{P} =} random_path(@var{ndim}, @var{delta})
## @defunx {[@var{P}, @var{B}] =} random_path(@dots{}, @var{B})
## Random path generation by one-at-a-time changes.
##
## Inputs:
## 
## @var{ndim} scalar, dimension of the input space.
## 
## @var{delta} scalar or 1-by-ndim vector for the step in the random path.
##
## @var{B} (optional) used to speed up calculations. 
## Pass the output of a previous call
##
## Outputs:
##
## @var{P} ndim+1-by-ndim random path, each row a new sample.
## 
## @var{B} used to speed up computation, pass it to a follow up call of this function
## to prevetn recomputation.
##
## Example:
## @example
## @group
## # 2 Random paths starting at 1,1,1
## ndim = 3;
## x0   = ones (1, ndim);
## X0   = repmat (x0, ndim + 1, 1);
## p    = zeros (ndim + 1, ndim, 2);
## for i = 1:2
##    p(:,:,i) = X0 + random_path(ndim, 0.25);
## endfor
## p
## @result{} p =
##
##ans(:,:,1) =
##
##   1.0000   1.0000   1.0000
##   1.0000   1.0000   1.2500
##   1.0000   1.2500   1.2500
##   0.7500   1.2500   1.2500
##
##ans(:,:,2) =
##
##   1.0000   1.0000   1.0000
##   1.0000   1.2500   1.0000
##   1.0000   1.2500   0.7500
##   1.2500   1.2500   0.7500
## @end group
## @end example
##
## @seealso{random_path_morris}
## @end defun

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>
## Created: 2021-03-08

function [pth, B] = random_path (ndim, delta=[], B=[])
  if isempty (B)
    B = tril(ones(ndim));
  endif
  if isempty(delta)
    delta = ones(1, ndim);
  endif
  
  ss  = 2 * (rand(1, ndim) > 0.5) - 1;
  pth = [zeros(1, ndim); B(:, randperm(ndim)).*ss] .* delta;
endfunction

%!demo
%! nlvl = 10;
%! ndim = 3;
%! npts = 10;
%! x0   = randi (nlvl, [npts, ndim]);
%! p    = zeros (ndim+1, ndim, npts);
%! for i = 1:npts
%!   p(:,:,i) = repmat (x0(i,:), ndim+1, 1) + random_path (ndim, 0.7);
%! endfor
%!
%! figure()
%! hold on
%! for i=1:npts
%!   h = plot3 (p(1,1,i), p(1,2,i), p(1,3,i), 'o');
%!   plot3(p(:,1,i), p(:,2,i), p(:,3,i), '-', 'color', get(h, 'color'))
%! endfor
%! plot3 (x0(:,1), x0(:,2), x0(:,3), 'k+');
%! hold off
%! axis equal
%!
%! grid on
%! xlabel ('x_1')
%! xlim ([0, nlvl+1]);
%! xticks (0:nlvl+1);
%! ylabel ('x_2')
%! ylim ([0, nlvl+1]);
%! yticks (0:nlvl+1);
%! zlabel ('x_3');
%! zlim ([0, nlvl+1]);
%! zticks (0:nlvl+1);
%! view(3)
%! #---------------------------------------------------------------------------
%! # Several random paths. Paths start exactly at the given seeds (marked with 
%! # crosses).
