## Copyright (C) 2021 JuanPi Carbajal <ajuanpi+dev@gmail.com>
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @defun {@var{N} =} neighborhood_vonneumann(@var{ndim})
## @defunx {@var{N} =} neighborhood_vonneumann(@var{ndim}), @var{delta})
## First order Von Neumann neighborhood in @var{ndim} dimensions.
##
## The von Neumann neighbourhood of a cell is the cell itself and the cells at a
## Manhattan distance of 1.
##
## References
## ----------
## @url{https://en.wikipedia.org/wiki/Von_Neumann_neighborhood}
##
## @end defun

function N = neighborhood_vonneumann(ndim, delta=[])
  if isempty(delta)
    delta = ones(1, ndim);
  endif
  N = [zeros(1, ndim); eye(ndim); -eye(ndim)] .* delta;
endfunction
