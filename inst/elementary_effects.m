## Copyright (C) 2021 JuanPi Carbajal
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @defun {@var{N} =} elementary_effects(@var{ndim})
##
##
## @end defun

function [e_eff, t_eff] = elementary_effects(model, x0, delta=[])

  [npts, ndim] = size (x0);
  Xs           = neighbors_vonneumann (x0, delta);
  m            = size (Xs, 1); % == 2 * dim + 1
  Xs_          = reshape (permute (Xs, [1,3,2]), [m * npts, ndim]);

  ys_          = model(Xs_);
  ys           = reshape (ys_, [m, npts]);

  dys     = ys(2:end,:) - ys(1,:);
  e_eff   = zeros (2 * ndim, npts);
  dim_idx = [0; ndim] + (1:ndim);
  dx      = delta .* [1; -1];

  for i=1:ndim
    e_eff(dim_idx(:,i),:) = dys(dim_idx(:,i), :) ./ dx(:, i);
  endfor

  % point of evaluation, dimension, direction of change (+, -)
  e_eff = permute (reshape (e_eff(dim_idx(:),:), 2, ndim, npts), [3,2,1]);

  # "An effective screening design for sensitivity analysis of large models"
  # https://doi.org/10.1016/j.envsoft.2006.10.004
  t_eff = mean (mean (abs (e_eff)), 3);
endfunction


%!demo
%! model = @(x) x(:,1) + 2 * x(:,2).^2;
%!
%! nlvl = 10;
%! ndim = 3;  # superfluous variable x_3
%! npts = 10;
%!
%! x0   = randi (nlvl, [npts, ndim]) / nlvl;
%!
%! delta = [1, 0.01, 2]; # always ndim array
%! [e_eff, t_eff] = elementary_effects(model, x0, delta);
%! printf('Total effects:\n')
%! disp(t_eff)
%! printf('Elementary effects:\n')
%! disp(e_eff)

