## Copyright (C) 2021 JuanPi Carbajal <ajuanpi+dev@gmail.com>
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @defun {@var{pts} =} neighbors_vonneumann(@var{centers})
## @defunx {@var{pts} =} neighbors_vonneumann(@var{centers}), @var{delta})
## First order Von Neumann neighbors of @var{centers}.
##
##
## @seealso{neighborhood_vonneumann}
##
## @end defun

function N = neighbors_vonneumann(x0, delta=[])

  [npts, ndim] = size (x0);
  x0           = reshape (x0.', [1, ndim, npts]);
  N            = zeros (2 * ndim + 1, ndim, 1);
  N(:,:,1)     = neighborhood_vonneumann (ndim, delta);
  N            = x0(1,:,:) + N;

endfunction

%!demo
%! nlvl = 10;
%! ndim = 3;
%! npts = 10;
%! x0   = randi (nlvl, [npts, ndim]);
%! xs   = neighbors_vonneumann (x0);
%!
%! UNO = ones(1,ndim*2);
%! figure()
%! hold on
%! for i=1:npts
%!   h = plot3 (xs(1,1,i), xs(1,2,i), xs(1,3,i), 'o', 'markerfacecolor', 'auto');
%!   line([xs(1,1,i) * UNO; xs(2:end,1,i).'],
%!         [xs(1,2,i) * UNO; xs(2:end,2,i).'],
%!         [xs(1,3,i) * UNO; xs(2:end,3,i).'],
%!         'color', get(h, 'color'))
%! endfor
%! hold off
%! axis equal
%!
%! grid on
%! xlabel ('x_1')
%! xlim ([0, nlvl+1]);
%! xticks (0:nlvl+1);
%! ylabel ('x_2')
%! ylim ([0, nlvl+1]);
%! yticks (0:nlvl+1);
%! zlabel ('x_3');
%! zlim ([0, nlvl+1]);
%! zticks (0:nlvl+1);
%! view(3)

