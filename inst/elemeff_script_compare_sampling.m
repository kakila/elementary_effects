## Global sensitvity via elementary effects
#
# *References*
#
# Campolongo, F., Cariboni, J., & Saltelli, A. (2007). 
# _An effective screening design for sensitivity analysis of large models_. 
# Environmental Modelling & Software, 22(10), 1509-1518. 
# <https://doi.org/10.1016/j.envsoft.2006.10.004>
#
# Morris, M. D. (1991). 
# _Factorial Sampling Plans for Preliminary Computational Experiments_. 
# Technometrics, 33(2), 161-174. 
# <https://doi.org/10.1080/00401706.1991.10484804>
#

##

# Copyright (C) 2021 - Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

pkg load elemeff


## Notation
# We will be making reference to a model with $k$ parameters and scalar 
# predictions, defining a $k+1$ dimensional manifold: $\mathbb{R}^{k} \rightarrow \mathbb{R}$.
#

## What do we want to compute?
# We want to perform a global sensitivity analysis on a model, assumed to be derivable.
# To do it, we sample the derivative of the model (approximated by finite differences) 
# in several points in the input space.
# We then evaluate some summary statistics on the distributions of the derivatives.
#
# Derivatives are obtained by changing one of the input parameters at a time (OAT).
# We can then study the distribution for the derivative w.r.t. each input.
#
# In the context of the articles referenced, the approaximation of the partial 
# derivative w.r.t the i-th input is called the *elementary effect* of the i-th 
# input.
# 
# There are to steps in this analysis:
#
# # Experimental design of samples (random sampling) 
# # Summary statistics of the distributions 
#
# Herein we compare several sampling strategies, and their effect on the 
# convergence of the summary statistics.
#

## Morris and Campolongo-Cariboni-Saltelli designs
# These design are based on generating a random point and then building a path  
# starting at it by moving one factor at a time in a random order.
# The elementary effects (partial derivatives) are then estimated by the difference
# ratio between two consecutive steps in the path.
#
# The starting points are chosen from a grid in the parameter space with a 
# predefined number of levels.
#
# The Morris'91 sampler have several deficiencies:
# - Paths can start arbirtrarily close to each other 
# - Paths do not start at the point given
#
# To alleviate these issues Campolongo-Cariboni-Saltelli proposed a new sampling
# strategy. The idea is to generate many trajectories using Morris'91 method and 
# then select the ones that ar emost separated in space (using Euclidean distance, 
# but other could be used).
# This is the same logic behind minmaxLHS sampling.
#
# The economy of these design is measured as the ratio between number of elementary
# effects and the number of sample sused to compute them.
# The samplers by Morris and Campolongo-Cariboni-Saltelli have an economy of
#
# $$\frac{r k}{r(k + 1)} = \frac{k}{k + 1}$$
#
# where $k$ is the dimension of the input and $r$ the number fo paths used.
# The denominator is due to the fact that each path has $k+1$ points.
#
# Herein we will compare Campolongo-Cariboni-Saltelli method with a different
# sampler using Von Nuemann neighborhoods.
#
# Below we see the demo of the Morris sampler, implemented in |random_path_morris|

demo random_path_morris

## Yet another random path design
# A simplification of morris algorithm is implemented in |random_path|.
# This sampler creates OAT paths that start in the seed points given
#

demo random_path


## Von Nuemann neighborhoods designs
# The idea is to estimate the partial derivatives w.r.t to each input in a neighborhood
# of the seed points given.
# For each seed point, we move each input up and down the given step. This produces
# $2 k + 1$ samples (two per input plus the seed point). And from each neighborhood
# we can compute $2 k$ elementary effects. This gives an economy of
#
# $\frac{2 r k}{r (2 k + 1)} = \frac{k}{k + \frac{1}{2}}$
#
# which is marginally better than the two other designs.
#
# Below we see the demo of this sampler, implemented in |neighbors_vonneumann|
#

demo neighbors_vonneumann

